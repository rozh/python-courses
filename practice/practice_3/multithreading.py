import time
import threading
import random

thread_life = True

def simple_thread(number:int):
    start_time = time.time()
    while thread_life:
        time.sleep(random.randint(0,2))
        print("Делаю вид, что работаю")
    end_time = time.time()
    print("Поток {} работал {} сек.".format(number, end_time - start_time))
    return None

t = threading.Thread(target=simple_thread, args=(1,))
t.start()
print("Запущен поток")
time.sleep(5)
thread_life = False
print("Останавливаем поток")
t.join()
print(t.is_alive())
