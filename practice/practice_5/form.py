import tkinter as tk

def btn_press():
    window = tk.Toplevel()
    window.title("")
    label = tk.Label(window, 
                text="Молодец!!!",
                font="arial 32")
    label.pack()

root = tk.Tk()
root.title("Окошко")

label = tk.Label(root, 
                text="hello world", 
                width=25, 
                height=1,
                bg="gold",
                fg="green",
                font="arial 26")
label.pack(side="right")

btn = tk.Button(root)
btn["text"] = "Нажми меня!!!!!!"
btn.pack(side="bottom")
btn["command"] = btn_press
tk.mainloop()