import tkinter as tk
import tkinter.messagebox as mb
import socket as s
import threading
import os


class ChatNameWindow(tk.Toplevel):
    def __init__(self, master):
        super().__init__(master)
        self.geometry("200x200")
        tk.Label(self, text="Введите имя:").pack()
        self.textBox = tk.Text(self, height=1)
        self.textBox.pack()
        tk.Button(self, text="OK", command=self.__setName).pack()

    def __setName(self):
        self.name = self.textBox.get("1.0", tk.END)[0:-1]
        self.destroy()


class ChatWindow(tk.Frame):
    def __init__(self, root, sock):
        super().__init__(root)
        self.root = root
        self.sock = sock
        self.grid(row=0, column=0, sticky="nswe")
        self.messages = tk.Text(self)
        self.messages.grid(row=0, column=0, columnspan=2, sticky="nswe")
        self.chat_message = tk.Text(self, height=1)
        self.chat_message.grid(row=1, column=0, sticky="nswe")
        self.btn = tk.Button(self, text="\u27A1", font="arial 24")
        self.btn.grid(row=1, column=1, columnspan=2, sticky="nswe")
        self.btn["command"] = self.sendMessage
        self.vscrollbar = tk.Scrollbar(
            self, orient='vert', command=self.messages.yview)
        self.messages['yscrollcommand'] = self.vscrollbar.set
        self.vscrollbar.grid(row=0, column=2, sticky="ns")
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        self.chat_message.bind("<Return>", self.sendMessage2)

        self.nameWindow = ChatNameWindow(root)
        self.nameWindow.grab_set()

    def sendMessage2(self, event):
        self.sendMessage()

    def sendMessage(self):
        msg = self.chat_message.get('1.0', tk.END)
        msg = self.nameWindow.name + ":" + msg
        self.chat_message.delete("1.0", tk.END)
        # lambda: self.chat_message.delete("1.0", tk.END))
        self.chat_message.after(10, self.deleteReturn)
        self.sock.sendall(msg.encode("utf-8"))

    def deleteReturn(self):
        self.chat_message.delete("1.0", tk.END)

    def receiveMessage(self, msg):
        self.messages.insert(tk.END, msg)
        self.messages.see(tk.END)


HOST = "localhost"  # адрес сервера чата
PORT = 34567        # порт сервера чата


def client_receive(soc: s.socket):
    """Поток получения сообщений
    """
    life = True
    while life:
        try:
            print("wait")
            data = soc.recv(4096)
            msg = data.decode("utf-8")
            print("\r<", end="")
            print(msg)
            print(">", end="", flush=True)  # flush
            chat.receiveMessage(msg)
        except Exception as ex:
            print("Ошибка получения сообщения:", ex)
            life = False
            soc.close()


soc = s.socket()
try:
    soc.connect((HOST, PORT))
except Exception as ex:
    print("Ошибка подключения:", ex)
    exit(1)

# запуск потока получения сообщений
receive_thread = threading.Thread(target=client_receive, args=(soc,))
receive_thread.start()

root = tk.Tk()
chat = ChatWindow(root, soc)
root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)
chat.mainloop()

# принудельное закрытие приложения с использованием средств операционной системы
pid = os.getpid()
os.kill(pid, 9)
