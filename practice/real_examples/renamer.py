import os


def replaceInFile(path: str, rename: dict):
    try:
        with open(path, 'r+', encoding='utf-8') as f:
            data = f.read()
            for key in rename:
                data = data.replace(key, rename[key])
            f.seek(0)
            f.truncate(0)
            f.write(data)
    except Exception as ex:
        print(path, ex)


def walkAndRename(path: str, rename: dict):
    for root, subdirs, files in os.walk(path, topdown=False):
        for file in files:
            filePath = os.path.join(root, file)
            replaceInFile(filePath, rename)
            for key in rename:
                if (file.find(key) >= 0):
                    os.rename(filePath, os.path.join(
                        root, file.replace(key, rename[key])))
                    break

        for subdir in subdirs:
            for key in rename:
                if (subdir.find(key) >= 0):
                    os.rename(os.path.join(root, subdir), os.path.join(
                        root, subdir.replace(key, rename[key])))
                    break


walkAndRename("C:\\Users\\roman\\Desktop\\Postamat\\src",
              {"CalculateCRM": "Postamat", "Crm": "Postamat"})
