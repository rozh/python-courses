import tkinter as tk
import random

class Shape():
    def __init__(self, id):
        self.x = 0
        self.y = 0
        self.color = "black"
        self.id = id

class Ball(Shape):
    def __init__(self, id, x, y, r):
        super().__init__(id)
        self.x = x
        self.y = y
        self.r = r
        self.yspeed = 0
        self.xspeed = 0

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Рисовашки")
        self.geometry("640x480")
        self.width = 640
        self.height = 480
        self.bind('<Configure>', self.resize)
        self.bind('<KeyPress>', self.on_key_press)
        self.bind('<KeyRelease>', self.on_key_release)
        self.balls = []
        self.keyPressed = {
            'Up':False,
            'Down':False,
            'Left':False,
            'Right':False,
            }

        self.__init_widgets()

    def __init_widgets(self):
        self.canvas = tk.Canvas(self, bg="white")
        self.canvas.bind("<Button-1>", self.canvas_on_click)
        self.canvas.pack(fill=tk.BOTH, expand=True)
        self.canvas.create_line(10, 10, 15, 35, fill="red")
        self.img = tk.PhotoImage(file="ball.gif")
        
        ball_id = self.canvas.create_image(100, 100, image=self.img, anchor='nw')
        self.player = Ball(ball_id, 125, 125, 25)
        self.balls.append(self.player)

        for _ in range(2):
            x = random.randint(25,400)
            y = random.randint(25,400)
            ball_id = self.canvas.create_image(x, y, image=self.img, anchor='nw')
            ball = Ball(ball_id, x+25, y+25, 25)
            ball.yspeed = random.randint(-2,2)
            ball.xspeed = random.randint(-2,2)
            self.balls.append(ball)
        
        self.animate()

    def animate(self):
        self.__input()
        ballNumber = 0
        for ball in self.balls:
            ballNumber += 1
            for i in range(ballNumber,len(self.balls)):
                otherBall = self.balls[i]
                balls_dest = (otherBall.x - ball.x)**2 +\
                    (otherBall.y - ball.y)**2
                if balls_dest <= (otherBall.r + ball.r)**2:
                    ball.xspeed, ball.yspeed, otherBall.xspeed, otherBall.yspeed = \
                        otherBall.xspeed, otherBall.yspeed, ball.xspeed, ball.yspeed

        for ball in self.balls:
            self.canvas.move(ball.id, ball.xspeed, ball.yspeed)
            ball.y += ball.yspeed
            ball.x += ball.xspeed

            if ball.x - ball.r < 0 or \
                ball.x + ball.r > self.width: 
                ball.xspeed = -ball.xspeed

            if ball.y - ball.r < 0 or \
                ball.y + ball.r > self.height: 
                ball.yspeed = -ball.yspeed

        
        self.canvas.after(33, self.animate)
    
    def __input(self):
        if self.keyPressed['Up']:
            self.player.yspeed = -3
        elif self.keyPressed['Down']:
            self.player.yspeed = 3
        else:
            self.player.yspeed = 0        

        if self.keyPressed['Left']:
            self.player.xspeed = -3
        elif self.keyPressed['Right']:
            self.player.xspeed = +3
        else:
            self.player.xspeed = 0
            
        

    def resize(self, event):
        self.width = event.width
        self.height = event.height

    def canvas_on_click(self, event):
        ballIds = self.canvas.find_withtag(tk.CURRENT)
        for ballId in ballIds:
            for ball in self.balls:
                if ball.id == ballId:
                    self.canvas.delete(ballId)
                    self.balls.remove(ball) 

    def on_key_press(self, event):
        self.keyPressed[event.keysym] = True

    def on_key_release(self, event):
        self.keyPressed[event.keysym] = False


if __name__ == "__main__":
    app = App()
    app.mainloop()
