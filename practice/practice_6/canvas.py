import tkinter as tk
import random

class Shape():
    def __init__(self, id):
        self.x = 0
        self.y = 0
        self.color = "black"
        self.id = id

class Ball(Shape):
    def __init__(self, id, x, y, r):
        super().__init__(id)
        self.x = x
        self.y = y
        self.r = r
        self.yspeed = 0
        self.xspeed = 0

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Рисовашки")
        self.geometry("640x480")
        self.width = 640
        self.height = 480
        self.bind('<Configure>', self.resize)
        self.balls = []

        self.__init_widgets()

    def __init_widgets(self):
        self.canvas = tk.Canvas(self, bg="white")
        self.canvas.pack(fill=tk.BOTH, expand=True)
        self.canvas.create_line(10, 10, 15, 35, fill="red")
        self.img = tk.PhotoImage(file="ball.gif")
        for _ in range(10):
            x = random.randint(25,400)
            y = random.randint(25,400)
            ball_id = self.canvas.create_image(x, y, image=self.img, anchor='nw')
            ball = Ball(ball_id, x+25, y+25, 25)
            ball.yspeed = random.randint(-15,15)
            ball.xspeed = random.randint(-15,15)
            self.balls.append(ball)
        
        self.animate()

    def animate(self):
        ballNumber = 0
        for ball in self.balls:
            ballNumber += 1
            for i in range(ballNumber,len(self.balls)):
                otherBall = self.balls[i]
                balls_dest = (otherBall.x - ball.x)**2 +\
                    (otherBall.y - ball.y)**2
                if balls_dest <= (otherBall.r + ball.r)**2:
                    ball.xspeed, ball.yspeed, otherBall.xspeed, otherBall.yspeed = \
                        otherBall.xspeed, otherBall.yspeed, ball.xspeed, ball.yspeed
            """
            otherBallIds = self.canvas.find_overlapping(
                ball.x - ball.r,
                ball.y - ball.r,
                ball.x + ball.r,
                ball.y + ball.r)
                
            for otherBallId in otherBallIds:
                for otherBall in self.balls:
                    if otherBall.id == otherBallId:
                        ball.xspeed, ball.yspeed, otherBall.xspeed, otherBall.yspeed = \
                            otherBall.xspeed, otherBall.yspeed, ball.xspeed, ball.yspeed
                
            
            ballNumber += 1
            for i in range(ballNumber,len(self.balls)):
                #otherBall = self.balls[i]
                #if ball != otherBall and \
                   #ball.x + ball.r > otherBall.x+otherBall.r  and \
                   #ball.y + ball.r > otherBall.y+otherBall.r  and \
                   #ball.x - ball.r < otherBall.x+otherBall.r and \
                   #ball.y - ball.r < otherBall.y+otherBall.r or \
                   #ball.x + ball.r > otherBall.x-otherBall.r  and \
                   #ball.y + ball.r > otherBall.y-otherBall.r  and \
                   #ball.x - ball.r < otherBall.x-otherBall.r and \
                   #ball.y - ball.r < otherBall.y-otherBall.r or \
                   #ball.x + ball.r > otherBall.x+otherBall.r  and \
                   #ball.y + ball.r > otherBall.y-otherBall.r  and \
                   #ball.x - ball.r < otherBall.x+otherBall.r and \
                   #ball.y - ball.r < otherBall.y-otherBall.r or \
                   #ball.x + ball.r > otherBall.x-otherBall.r  and \
                   #ball.y + ball.r > otherBall.y+otherBall.r  and \
                   #ball.x - ball.r < otherBall.x-otherBall.r and \
                   #ball.y - ball.r < otherBall.y+otherBall.r:
                    items = self.canvas.find_overlapping(
                        ball.x - ball.r,
                        ball.y - ball.r,
                        ball.x + ball.r,
                        ball.y + ball.r)
                    ball.xspeed, ball.yspeed, otherBall.xspeed, otherBall.yspeed = \
                        otherBall.xspeed, otherBall.yspeed, ball.xspeed, ball.yspeed
                        """

        for ball in self.balls:
            self.canvas.move(ball.id, ball.xspeed, ball.yspeed)
            ball.y += ball.yspeed
            ball.x += ball.xspeed

            if ball.x - ball.r < 0 or \
                ball.x + ball.r > self.width: 
                ball.xspeed = -ball.xspeed

            if ball.y - ball.r < 0 or \
                ball.y + ball.r > self.height: 
                ball.yspeed = -ball.yspeed

        
        self.canvas.after(33, self.animate)

    def resize(self, event):
        self.width = event.width
        self.height = event.height

if __name__ == "__main__":
    app = App()
    app.mainloop()
