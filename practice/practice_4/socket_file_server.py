import socket

s = socket.socket()

s.bind(("0.0.0.0",12345))
s.listen(1)

c, adr = s.accept()
with open("file.jpg", "wb") as f:
    data = c.recv(4096)
    while len(data) > 0:
        print("Получено {} байт".format(len(data)))
        f.write(data)
        data = c.recv(4096)
    c.close()

s.close()