import socket as s
import threading
import os

HOST = "localhost"  # адрес сервера чата
PORT = 34567        # порт сервера чата


def client_receive(soc: s.socket):
    """Поток получения сообщений
    """
    life = True
    while life:
        try:
            data = soc.recv(4096)
            print("\r<", end="")
            print(data.decode("utf-8"))
            print(">", end="", flush=True)  # flush
        except Exception as ex:
            print("Ошибка получения сообщения:", ex)
            life = False
            soc.close()


soc = s.socket()
try:
    soc.connect((HOST, PORT))
except Exception as ex:
    print("Ошибка подключения:", ex)
    exit(1)

# запуск потока получения сообщений
receive_thread = threading.Thread(target=client_receive, args=(soc,))
receive_thread.start()

while True:
    try:
        msg = input(">")
    except KeyboardInterrupt:
        msg = "\\quit"
    except:
        print("Ошибка ввода")

    if msg == "\\quit":
        break

    try:
        soc.sendall(msg.encode("utf-8"))
    except:
        print("Ошибка отправки данных")

# принудельное закрытие приложения с использованием средств операционной системы
pid = os.getpid()
os.kill(pid, 9)
