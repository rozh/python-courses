import socket as s
import threading as t
import os

HOST = "0.0.0.0"
PORT = 34567  # 1024 - 59000
clients = []


def sendMesssageAllOthers(msg, client_soc: s.socket):
    for client in clients:
            client.sendall(msg)


def client_thread(client_soc: s.socket, client_addr):
    '''Поток обслуживания клиента
    '''
    life = True
    while life:
        try:
            msg = client_soc.recv(4096)
            print(msg.decode("utf-8"))
            sendMesssageAllOthers(msg, client_soc)
        except:
            life = False
            client_soc.close()
            print("Клиент {}:{} отключился".format(*client_addr))

    clients.remove(client_soc)


def client_accept_thread(server_soc: s.socket):
    '''Приема соединений
    '''
    while True:
        client_soc, client_addr = soc.accept()
        clients.append(client_soc)
        print("Новое подключение от {}:{}".format(*client_addr))
        clinet_t = t.Thread(target=client_thread,
                            args=(client_soc, client_addr))
        clinet_t.start()


soc = s.socket()
soc.bind((HOST, PORT))
soc.listen(1)

accept_thread = t.Thread(target=client_accept_thread, args=(soc,))
accept_thread.start()

input("Для выхода нажмите Enter")

# принудельное закрытие приложения с использованием средств операционной системы
pid = os.getpid()
os.kill(pid, 9)
