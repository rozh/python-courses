class Matrix:
    '''
    Класс для работы с матрицами
    '''

    def __init__(self, matrix: list):
        self.matrix = matrix

    def __add__(self, otherMatrix):
        '''
        m3 = m1 + m2, где mn - Matrix
        '''
        result = []
        for i in range(len(self.matrix)):
            row = []
            for j in range(len(self.matrix[i])):
                s = self.matrix[i][j] + otherMatrix.matrix[i][j]
                row.append(s)
            tup = tuple(row)
            result.append(tup)
        return Matrix(result)

    def __str__(self):
        s = ""
        for row in self.matrix:
            for el in row:
                s += str(el) + "\t"
            s += "\n"
        return s

    def save(self, filename: str):
        with open(filename, "w") as f:
            #f.write(self.__str__())
            for row in self.matrix:
                for el in row:
                    f.write(str(el))
                    f.write("\t")
                f.write("\n")

    @staticmethod
    def load(filename: str):
        '''
        Возвращяет объект Matrix
        '''
        matrix = []
        try:
            with open(filename) as f:
                for line in f:
                    row = []
                    if line.endswith("\n"):
                        line = line[0:-1]
                    rowStr = line.split("\t")
                    for el in rowStr:
                        row.append(float(el))
                    tup = tuple(row)
                    matrix.append(tup)
        except IOError:
            print("Ошибка доступа к файлу. Проверьте правильность пути и наличие файла")
        except Exception as ex:
            print("Все пропало, шеф!!!")
            print(ex)
        else:
            print("Матрица успешно загружена.")

        return Matrix(matrix)
