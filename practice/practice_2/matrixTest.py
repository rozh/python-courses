import classes as c

m1 = [
    (121, 232, 33, 4),
    (13, 22, 32, 43),
    (1, 2, 3, 42),
]

mat1 = c.Matrix(m1)
mat2 = c.Matrix.load("matrix.txt")

print(mat2)

mat3 = mat1 + mat2
mat3.save("mat3.txt")