import sqlite3


def db_init(con: sqlite3.connect):
    """Инициализация БД:
    - создание таблиц
    - заполнение данными
    """

    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS Students  (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            age INTEGER,
            group_id INTEGER
            )""")

    cur.execute("""CREATE TABLE IF NOT EXISTS "Groups" (
                "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
                "name"	TEXT
                )""")

    cur.execute("select count(id) from Groups")
    groups_count = cur.fetchone()[0]

    if groups_count == 0:
        cur.execute("""INSERT INTO Groups (name) VALUES 
                    ('4401'),
                    ('4402')
                    """)
        con.commit()

    cur.execute("select count(id) from Students")
    students_count = cur.fetchone()[0]

    if (students_count == 0):
        students = [
            ('Вася', 20, 1),
            ('Лена', 21, 2),
            ('Коля', 19, 1),
        ]
        cur.executemany("""INSERT INTO Students 
                        (name, age, group_id) VALUES 
                        (?, ?, ?)
                        """, students)
        con.commit()
    cur.close()


con = sqlite3.connect('example.db')
db_init(con)

cur = con.cursor()

cur.execute("""select s.id, s.name, g.name
            from 
                Students as s join Groups as g 
                    on s.group_id = g.id
            """)
students = cur.fetchall()
for student in students:
    print(*student)

cur.execute("""delete from students where id = 3""")
cur.execute("""update groups set name = '4403' 
                where name = '4401'""")
con.commit()

print("After delete")

cur.execute("""select s.id, s.name, g.name
            from 
                Students as s join Groups as g 
                    on s.group_id = g.id
            """)
students = cur.fetchall()
for student in students:
    print(*student)

cur.close()
con.close()