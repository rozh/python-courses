import matplotlib.pyplot as plt

data = []
with open("data.txt") as f:
    for line in f:
        items = line.split(" ")
        float_items = []
        for item in items:
            float_items.append(float(item.replace(",",".")))
        data.append(tuple(float_items))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, aspect=1)
x, y = zip(*data)
ax.plot(x,y,linewidth=1, marker='o', markerfacecolor='w', markeredgecolor='k')
plt.tight_layout()
plt.show()