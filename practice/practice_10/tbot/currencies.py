from cbrf.models import DailyCurrenciesRates

def get_exchanges(currencies: list) -> list:
    daily = DailyCurrenciesRates()
    result = []
    for rate in daily.rates:
        for currency in currencies:
            if (currency.lower() == rate.char_code.lower()):
                result.append(f"{rate.value} р. за {rate.denomination} {rate.char_code}")
    return result


if __name__ == "__main__":
    data = get_exchanges(['USD',"EUR"])
    print(*data, sep="\n")