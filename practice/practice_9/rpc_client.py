import socket as s
import threading
import os
import rpc_common

HOST = "localhost"  # адрес сервера чата
PORT = 12346        # порт сервера чата


soc = s.socket()
try:
    soc.connect((HOST, PORT))
except Exception as ex:
    print("Ошибка подключения:", ex)
    exit(1)

while True:
    try:
        msg = input(">")
    except KeyboardInterrupt:
        msg = "\\quit"
    except:
        print("Ошибка ввода")

    if msg == "\\quit":
        break

    try:
        soc.sendall(rpc_common.create_rpc_packet(b'f', msg))
    except:
        print("Ошибка отправки данных")

# принудельное закрытие приложения с использованием средств операционной системы
pid = os.getpid()
os.kill(pid, 9)
