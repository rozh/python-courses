import socket as s
import threading as t
import struct 
import os
import rpc_common

clients = []

def hello():
    print("hello world")

def client_thread(client_soc: s.socket, client_addr):
    '''Поток обслуживания клиента
    '''
    life = True
    while life:
        try:
            msg = client_soc.recv(4096)
            tup = rpc_common.unpack_rpc_packet(msg)
            if (tup[0]==b'f'):
                funct_name = tup[1].decode("utf-8").strip("\0")
                if (funct_name == "hello"): hello()
        except:
            life = False
            client_soc.close()
            print("Клиент {}:{} отключился".format(*client_addr))

    clients.remove(client_soc)

def client_accept_thread(server_soc: s.socket):
    '''Приема соединений
    '''
    while True:
        client_soc, client_addr = soc.accept()
        clients.append(client_soc)
        print("Новое подключение от {}:{}".format(*client_addr))
        clinet_t = t.Thread(target=client_thread,
                            args=(client_soc, client_addr))
        clinet_t.start()

HOST = "0.0.0.0"
PORT = 12346

soc = s.socket()
soc.bind((HOST, PORT))
soc.listen(1)

accept_thread = t.Thread(target=client_accept_thread, args=(soc,))
accept_thread.start()

input("Для выхода нажмите Enter")

# принудельное закрытие приложения с использованием средств операционной системы
pid = os.getpid()
os.kill(pid, 9)