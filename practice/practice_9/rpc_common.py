import struct

#формат сообщения
rpc_packet_format = "c50s"


def create_rpc_packet(ptype :bytes, funct_name: str):
    '''Упаковка значений в массив байт'''
    funct_name_bytes = funct_name.encode("utf-8")
    data = struct.pack(rpc_packet_format, 
                       ptype, 
                       funct_name_bytes)
    return data

def unpack_rpc_packet(data):
    '''Распаковка значений из массива байт в массив значений'''
    return struct.unpack(rpc_packet_format, data)


if __name__ == "__main__":
    d = create_rpc_packet(b'f', "hello")
    tup = unpack_rpc_packet(d)