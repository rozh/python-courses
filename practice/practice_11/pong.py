import tkinter as tk
import tkinter.messagebox as mb
import random
import socket
import threading
import struct
import os


class GameState():
    player1_y = 150
    player2_y = 150
    ball_x = 250
    ball_y = 150
    ball_r = 10
    ball_x_speed = 2
    ball_y_speed = 2
    player1_score = 0
    player2_score = 0

    packet_format = "hhbbbhhhh"

    def serialize(self) -> bytes:
        data = struct.pack(self.packet_format,
                           int(self.ball_x),
                           int(self.ball_y),
                           int(self.ball_r),
                           int(self.ball_x_speed),
                           int(self.ball_y_speed),
                           int(self.player1_y),
                           int(self.player2_y),
                           int(self.player1_score),
                           int(self.player2_score))
        return data

    def deserialize(self, data: bytes):
        tup = struct.unpack(self.packet_format, data)
        self.ball_x, \
            self.ball_y, \
            self.ball_r, \
            self.ball_x_speed, \
            self.ball_y_speed, \
            self.player1_y, \
            self.player2_y, \
            self.player1_score, \
            self.player2_score = tup


class ConnectWindow(tk.Toplevel):
    def __init__(self, root):
        super().__init__(root)
        self.address = '127.0.0.1'
        self.title("Соединение с сервером")
        self.resizable(False, False)
        self.geometry("300x60")
        self.label = tk.Label(self, text="Введите адрес сервера")
        self.label.pack(side=tk.TOP)
        self.ip_input = tk.Text(self, height=1, width=25)
        self.ip_input.insert(1.0, self.address)
        self.ip_input.pack(side=tk.LEFT)
        self.btn = tk.Button(self, command=self.btn_click, text="Соединиться")
        self.btn.pack(side=tk.RIGHT)

    def btn_click(self):
        self.address = self.ip_input.get("1.0", tk.END)[0:-1]
        self.destroy()


class Game(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Pong")
        self.address = ""
        self.resizable(False, False)
        self.width = 500
        self.height = 300
        self.is_server = True
        self.geometry(f"{self.width}x{self.height}")
        self.canvas = tk.Canvas(self, bg="green4")
        self.canvas.pack(fill=tk.BOTH, expand=True)
        self.game_state = GameState()
        self.out_line = 25
        self.player_height = 80
        self.bind('<KeyPress>', self.on_key_press)
        self.bind('<KeyRelease>', self.on_key_release)
        self.keyPressed = {
            'Up': False,
            'Down': False,
            'ClientUp': False,
            'ClientDown': False
        }
        self.__start()

    def __start(self):
        self.is_server = not mb.askyesno("ВНИМАНИЕ!!!",
                                         "Подключиться к серверу?")
        if self.is_server:
            self.__init_server()
            self.__restart()
            self.__game_run()
        else:
            win = ConnectWindow(self)
            win.wait_visibility()
            win.grab_set()
            self.wait_window(win)
            self.address = win.address
            print(self.address)
            self.__init_client()
            self.__game_run_client()

    def __init_server(self):
        self.sock = socket.socket()
        self.sock.bind(("0.0.0.0", 12345))
        self.sock.listen(1)
        client_soc, _ = self.sock.accept()
        self.client_socket = client_soc
        self.receive_thread = threading.Thread(target=self.__server_recieve)
        self.receive_thread.start()

    def __init_client(self):
        self.sock = socket.socket()
        self.sock.connect((self.address, 12345))
        self.receive_thread = threading.Thread(target=self.__client_recieve)
        self.receive_thread.start()

    def __client_recieve(self):
        while True:
            data = self.sock.recv(16)
            self.game_state.deserialize(data) 
            self.canvas.after(1, self.__game_run_client) # рисуем только после полуения состояния

    def __server_recieve(self):
        while True:
            data = self.client_socket.recv(1)
            if data == b'U' or data == b'u':
                self.keyPressed['ClientUp'] = data == b'U'
            elif data == b'D' or data == b'd':
                self.keyPressed['ClientDown'] = data == b'D'

    def on_key_press(self, event):
        self.keyPressed[event.keysym] = True
        if not self.is_server:
            if self.keyPressed['Up']:
                self.sock.sendall(b'U')
            elif self.keyPressed['Down']:
                self.sock.sendall(b'D')

    def on_key_release(self, event):
        self.keyPressed[event.keysym] = False
        if not self.is_server:
            if not self.keyPressed['Up']:
                self.sock.sendall(b'u')
            if not self.keyPressed['Down']:
                self.sock.sendall(b'd')

    def __input(self):
        if self.keyPressed['Up']:
            self.game_state.player1_y -= 10
        elif self.keyPressed['Down']:
            self.game_state.player1_y += 10

        if self.keyPressed['ClientUp']:
            self.game_state.player2_y -= 10
        elif self.keyPressed['ClientDown']:
            self.game_state.player2_y += 10

    def __restart(self):
        gs = self.game_state
        gs.player1_y = self.height/2
        gs.player2_y = self.height/2
        gs.ball_x = self.width/2
        gs.ball_y = self.height/2
        gs.ball_y_speed = random.randint(-3, 3)
        gs.ball_x_speed = random.randint(-3, 3)
        if gs.ball_x_speed == 0:
            gs.ball_x_speed = 2

    def __update(self):
        gs = self.game_state
        gs.ball_y += gs.ball_y_speed
        gs.ball_x += gs.ball_x_speed

        if gs.ball_y - gs.ball_r < 0 or \
                gs.ball_y + gs.ball_r > self.height:
            gs.ball_y_speed = -gs.ball_y_speed

        if gs.ball_x - gs.ball_r < self.out_line or \
                gs.ball_x + gs.ball_r > self.width-self.out_line:

            if (gs.ball_x > self.width/2):
                # player 2
                if gs.ball_y <= gs.player2_y + self.player_height/2 and \
                        gs.ball_y >= gs.player2_y - self.player_height/2:
                    gs.ball_x_speed = -gs.ball_x_speed
                    speed_increase = random.randint(1, 3)
                    gs.ball_x_speed -= speed_increase
                    if gs.ball_y_speed > 0:
                        gs.ball_y_speed += speed_increase
                    else:
                        gs.ball_y_speed -= speed_increase
                else:
                    gs.player1_score += 1
                    self.__restart()
            else:
                # player 1
                if gs.ball_y <= gs.player1_y + self.player_height/2 and \
                        gs.ball_y >= gs.player1_y - self.player_height/2:
                    gs.ball_x_speed = -gs.ball_x_speed
                    speed_increase = random.randint(1, 3)
                    gs.ball_x_speed += speed_increase
                    if gs.ball_y_speed > 0:
                        gs.ball_y_speed += speed_increase
                    else:
                        gs.ball_y_speed -= speed_increase
                else:
                    gs.player2_score += 1
                    self.__restart()

        self.client_socket.sendall(gs.serialize())

    def __draw(self):
        self.canvas.delete("all")
        gs = self.game_state
        self.canvas.create_oval(
            gs.ball_x-gs.ball_r,
            gs.ball_y-gs.ball_r,
            gs.ball_x+gs.ball_r,
            gs.ball_y+gs.ball_r,
            fill="yellow"
        )

        self.canvas.create_rectangle(
            10,
            gs.player1_y - self.player_height/2,
            self.out_line,
            gs.player1_y + self.player_height/2,
            fill="red"
        )

        self.canvas.create_rectangle(
            self.width-self.out_line,
            gs.player2_y - self.player_height/2,
            self.width-10,
            gs.player2_y + self.player_height/2,
            fill="blue"
        )

        self.canvas.create_text(self.width/2, 20,
                                text=f"{self.game_state.player1_score}:{self.game_state.player2_score}",
                                justify=tk.CENTER, font="Arial 16")

    def __game_run(self):
        self.__input()
        self.__update()
        self.__draw()

        self.canvas.after(33, self.__game_run)

    def __game_run_client(self):
        self.__draw()


game = Game()
game.mainloop()

pid = os.getpid()
os.kill(pid, 9)
