# Требования к оформлению пояснительной записки выпускной работы

Объем поснительной записки - от 10 до 15 стр.

Формат для электронного представления - PDF.

Пример оформления работы: [в docx](example.docx), в [pdf](example.pdf)

## Структура пояснительной записки

- [Титульный лист](title.docx)
- Оглавление
- Введние и постановка задачи
- Поэтапное решение задачи
- Заключение (в процессе выполнения работы были изучены ...)

## Правила оформления пояснительной записки

Текстовые документы представляются на белой бумаге формата А4. Печать  материалов  односторонняя  с  соблюдением  следующих  размеров  полей: левое – 3 см (место для подшивки), правое – 1,5 см, верхнее и нижнее – 2 см, размер  шрифта  14,  Times  New  Roman  или  Arial,  междустрочный  интервал – полторы строки. Применяется книжная ориентация страниц, однако при необходимости  допускается  оформление  отдельных  страниц  в  альбомной  ориентации. При  этом  все  надписи,  включая  номер  страницы,  «поворачиваются»  вместе  с поворотом страницы, а верх страницы располагается под переплётом. 

Титульный лист оформляется по [шаблону бланка](title.docx), перенос  слов  не  разрешается,  точки  в  конце  названий  не  ставятся.  Титульный лист  также  составляется  на  иностранном  (как  правило,  английском)  языке  и помещается в ПЗ сразу за основным титульным листом. При этом  титульный лист на иностранном языке в общей нумерации страниц не участвует. 

Текст ПЗ – печатный (одноцветное представление). «Висячие» строки исключаются.  Исправления  в  тексте  (отдельные  слова,  символы  или  формулы) вписывать пастой одного цвета. При большом количестве исправлений на странице (пять и более) она должна быть переработана. 

Нумерация  страниц  работы  выполняется  арабскими  цифрами  справа вверху страницы. Учёт нумерации страниц начинается с титульного листа, но номера страниц на титульном листе не  ставятся.  Знаки  номера  страницы  появляются  с  оглавления. 

### Оформление иллюстраций, таблиц, формул 

Рисунки, схемы, графики, диаграммы выполняются на белой бумаге черной пастой. Разрешается выполнять иллюстрации в любых цветах на цветном принтере,  обеспечивающем  хорошее  качество  печати.  Допустимо  также  подклеивать  фотографии,  иллюстрации  и  другие  материалы,  которые  не  могут быть выполнены на белой писчей бумаге на листы бумаги пояснительной записки.  Кроме  формата  А4  для  иллюстраций  (включая  таблицы)  разрешается использовать бумагу большего формата, например, A3. Такой лист складывается соответствующим образом до формата, используемого в пояснительной записке и при этом нумеруется только как одна страница.

Каждая иллюстрация должна иметь наименование, а при необходимости и поясняющие данные, которые располагаются под ней. Переносы слов в подписи к иллюстрациям не допускаются. Точка в конце подписи к иллюстрации не ставится. 

Иллюстрации  (рисунки,  схемы,  графики,  фотографии  и  т.д.)  имеют сплошную нумерацию. Арабскими цифрами указывается номер иллюстрации, например, первый рисунок будет нумероваться, как Рис. 1. Каждый вид иллюстрации (рисунки, таблицы, формулы) имеет свою отдельную систему нумерации. 

Обозначение «Таблица...» ставится над заголовком соответствующей таблицы  справа.  Заголовок  таблицы  располагается  на  следующей  строке по центру.  При  расположении  таблицы  на  нескольких  страницах  номер  таблицы  и 
шапка дублируются на все страницы, на которых она располагается. 

Рисунки (графики, фотографии, схемы) располагаются по центру страницы. Подпись располагается под рисунком также по центру. Обязательно указывается название рисунка. 

Формулы  вводятся  при  помощи  специальных  инструментов,  типа  Microsoft Equation или аналогичных. Допустимо вставлять рисунок с изображением формулы, при условии её чёткого отображения при размере шрифта соизмеримом с основным. 

Формулы располагаются по центру страницы. Номер указывается с правой стороны листа на уровне формулы в круглых скобках. После самой формулы описываются наименования и значения параметров и индексов, если ранее они не встречались. 