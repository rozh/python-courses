import random


class Brick():
    '''
    Кирпич
    '''

    def __init__(self, height=0, width=0, length=0):
        if height == 0:
            self.__height = random.randint(1, 10)
        else:
            self.__height = height
        if width == 0:
            self.__width = random.randint(1, 10)
        else:
            self.__width = width
        if length == 0:
            self.__length = random.randint(1, 10)
        else:
            self.__length = length

    def getVolume(self):
        return self.__width * self.__height * self.__length

    def setHeight(self, height):
        self.__height = height

    def __str__(self):
        return "Кирпич {} на {} на {} см" \
            .format(self.__height,
                    self.__width,
                    self.__length)


class WallBrick:
    def __init__(self, *bricks: Brick):
        self.__bricks = list(bricks)

    def addBrick(self, brick: Brick):
        self.__bricks.append(brick)

    @property
    def bricks(self):
        return self.__bricks

    @property
    def bricksVolume(self):
        return self.__bricks

    @bricksVolume.getter
    def bricksVolume(self):
        sumV = 0
        for b in self.__bricks:
            sumV += b.getVolume()
        return sumV

    def __add__(self, otherWall):
        wall1_bricks = self.__bricks
        wall2_bricks = otherWall.bricks
        wall_bricks = wall1_bricks + wall2_bricks
        return WallBrick(*wall_bricks)

    def printBricksCount(self):
        print("Количество кирпичей в стене",
              len(self.__bricks))

if __name__ == "__main__":
    print(Brick())

    brick = Brick(4, 6, 10)
    bricks = []
    for i in range(100):
        bricks.append(Brick(4, 6, 10))

    wall1 = WallBrick(*bricks)
    wall1.printBricksCount()
    wall2 = WallBrick(brick, brick)
    wall2.printBricksCount()
    wall = wall1 + wall2
    wall.printBricksCount()
    print("Объем кирпичей =", wall.bricksVolume)
