import classes as c


brick = c.Brick()
bricks = []
for i in range(10):
    bricks.append(c.Brick())

wall1 = c.WallBrick(*bricks)
wall1.printBricksCount()
wall2 = c.WallBrick(brick, brick)
wall2.printBricksCount()
wall = wall1 + wall2
wall.printBricksCount()
print("Объем кирпичей =", wall.bricksVolume)